
module Sum_N(
  input [2:0] N_in,
  input N_valid,
  input clk,
  input ack,
  input reset,
  output ready,
  output  [4:0] Sum,
  output Sum_valid
);
  wire i_eq_1;
  wire [1:0]state;
  datapath datapath_instance (.N_in(N_in),.i_eq_1(i_eq_1),.clk(clk),
                             .Sum(Sum),.state(state));
  
  controlpath control_instance (.N_valid(N_valid),.ack(ack),
                               .reset(reset),.clk(clk),
                                .ready(ready),.i_eq_1(i_eq_1),
                               .Sum_valid(Sum_valid),.state(state)); 
  
 endmodule 
  
  
  module datapath
    (
    input [2:0] N_in,
    input clk,
     
  
    output reg [4:0] Sum,
    input [1:0]state,
    output i_eq_1
    );
   parameter  IDLE = 2'b00;
   parameter  BUSY = 2'b01;
   parameter  DONE = 2'b10;
  
    
  reg [2:0] i; 
  reg [2:0] i_mux_out;
  wire [4:0] add_out;
  
  
  reg [4:0] sum_mux_out;
  wire i_eq_1;
  reg [1:0] i_mux_sel,sum_mux_sel;
  wire i_eq_1_state;
 
      //Comparator with rtl bug fixing when N=0 it will give Sum = 0
  assign i_eq_1 = (i==1 || i==0);
  
  
   // Adder
  assign add_out = Sum + i;
  
  
  //2:1 Multiplexer at the input of sum register
  always @(*)
    begin
      case(sum_mux_sel)
        2'b00 : sum_mux_out = 0;
        2'b01 : sum_mux_out = add_out;
        2'b10 : sum_mux_out = Sum;
        default : sum_mux_out= {5{1'bx}};
      endcase
    end
        
        
  // 2:1 Multiplexer at the input of i register      
   always @(*)
    begin
      case(i_mux_sel)
        2'b00 : i_mux_out = N_in;
        2'b01 : i_mux_out = (i - 1);
        2'b10 : i_mux_out = i;
        default : i_mux_out= {3{1'bx}};
      endcase
    end
        
    // Sum, i, registers
  
  always @(posedge clk )
      begin
       Sum <= sum_mux_out;
       i <= i_mux_out;
       
      end
   //state decode
   always @(*)
          begin
            case(state)
              IDLE:
                begin
                  sum_mux_sel = 2'b00;
                  i_mux_sel = 2'b00;
                end
              BUSY:
                begin
                  sum_mux_sel = 2'b01;
                  i_mux_sel = 2'b01;
                end
              DONE:
                begin
                  sum_mux_sel = 2'b10;
                  i_mux_sel = 2'b10;
                end
            endcase
          end
        
  endmodule
  
  
  module controlpath
    (
    
  input N_valid,
  input clk,
  input ack,
  input reset,
  output ready,
  output reg [4:0] Sum,
  output Sum_valid,
  output reg [1:0]state,
  input i_eq_1
    
    );
    reg [1:0]nextstate;
 
     parameter  IDLE = 2'b00;
   parameter  BUSY = 2'b01;
   parameter  DONE = 2'b10;
    
    //it is AND gate
   assign Sum_valid = (state == DONE);
    assign ready = (state == IDLE);
 
    // state machine 
    always @(*)
        begin          
          case(state)           
            IDLE: if(N_valid) nextstate = BUSY; else nextstate = IDLE;                 
            BUSY: if(i_eq_1) nextstate = DONE; else nextstate = BUSY;              
            DONE: if(ack) nextstate = IDLE;  else nextstate = DONE;            
          endcase
        end
        
  always @(posedge clk or posedge reset)
    if(reset)
      state <= IDLE;
    else
      state <= nextstate;
  endmodule
  
  
  
  
  
        
   
         
              

