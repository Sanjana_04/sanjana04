module sum_N_tb;
  //inputs
  
  reg [2:0]N_in;
  reg clk,reset,ack,N_valid;
  
  //outputs
  wire Sum_valid,ready;
  wire [4:0]Sum;
  
  reg [4:0]sum_behav_out;
  integer i;
  Sum_N Sum_V_instance(.N_in(N_in),
               .ack(ack),
               .ready(ready),
               .N_valid(N_valid),
               .clk(clk),
               .reset(reset),
            .Sum(Sum),
            .Sum_valid(Sum_valid));
  
    
    
  function [4:0] sum_behav (input [2:0] N);
    sum_behav = (N*(N+1))/2;
  endfunction 
 
    always # 5 clk=~clk;   

   initial begin
      clk=0;
      reset=0;
       N_valid=0;
       N_in=0;
       ack=0;     
      
       #10 reset=1;
       #10 reset=0;
       #10
       N_in = $urandom % 7;
       N_valid = 1;
       #20
       N_valid =0;
       #80
      ack =1;
     
       
   	wait(Sum_valid == 1)
     
      begin
   		sum_behav_out = sum_behav(N_in);    
   
        if (sum_behav_out == Sum)
    	$display("Test Passed");
   	   else
   		$display("Test Failed");
      end
    
  		#200
  		$finish;
     end
  
  
   initial begin
    // Dump waves
    $dumpfile("dump.vcd");
    $dumpvars(0);
    end
  
endmodule
