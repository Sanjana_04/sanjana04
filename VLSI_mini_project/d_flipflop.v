module d_flipflop(d_feedback, clk, enable, reset, out);
  input d_feedback, clk, enable, reset;
  output out;

  reg out;

  always @ (posedge clk or posedge reset) begin
    if (reset)
      out = 0;
    else
      if (enable)
        out = d_feedback;
  end
endmodule
