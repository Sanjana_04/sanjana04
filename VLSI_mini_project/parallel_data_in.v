module parallel_data_in(clk, enable, reset, data, out);
  parameter data_width = 32;  			
  
  input enable, clk, reset;
  input [data_width-1:0] data;
  output reg out;
  
  reg [data_width-1:0] memory_register;

  always @ (posedge clk, posedge reset) begin
    
    if (reset == 1) begin
      out <= 0;
      memory_register <= data;
    end
    
    else begin
      if (enable) begin
        out = memory_register[0];
        memory_register = memory_register >> 1'b1;
      end
    end
    
  end
endmodule
