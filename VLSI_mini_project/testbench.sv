module serial_adder_tb;
  parameter data_width = 32,
  			width_out = 33;
  			
  reg [data_width - 1:0] input_a,input_b;
  reg clk, reset;

  wire [width_out-1:0] sum;
  wire cout;

  serial_adder SA(input_a, input_b, clk, reset, sum, cout);
  
  initial begin
  $dumpfile("dump.vcd");
  $dumpvars(0);
 end

  initial begin
    $monitor("input_a = %32b, input_b = %32b, reset = %b, sum=%33b", input_a, input_b, reset, sum);
   
    clk = 0;
    input_a = 32'b10000000111100000000000000000001; 
    input_b = 32'b10101010101010101000001010101011; 
    reset = 1; #20;
    reset = 0; #500;
    $finish;
  end

  always #5 clk = !clk;

endmodule
