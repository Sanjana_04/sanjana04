// Code your design here
`include "piso.v"
`include "d_flipflop.v"
`include "full_adder.v"

module serial_adder(data_a, data_b, clk, reset, out, cout);
parameter data_width = 4,
  			count_width = 3;
  input [data_width -1 :0] data_a, data_b;
  input clk, reset;
  output cout;
  output [data_width -1 :0] out;

  reg [data_width -1 :0] out;
  reg [count_width - 1:0] count;
  reg enable, cout;
  wire wire_a, wire_b, cout_temp, cin, sum;

  piso piso_a(clk, enable, reset, data_a, wire_a);
  piso piso_b(clk, enable, reset, data_b, wire_b);
  full_adder adder(wire_a, wire_b, cin, sum, cout_temp);
  d_flipflop dff(cout_temp, clk, enable, reset, cin);

  always @ (posedge clk or posedge reset) begin
    if (reset) begin
      enable = 1; count = 0; out = 0;
    end
    else begin
      if (count > 4)
        enable = 0;
      else begin
        if (enable) begin
          cout = cout_temp;
          count = count + 1;
          out = out >> 1;
          out[3] = sum;
        end
      end
    end
  end
endmodule
